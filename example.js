const { enphase, solaredge } = require('./src');
const { getDailyProduction: getEnphaseDailyProduction } = enphase;
const { getDailyProduction: getSolaredgeDailyProduction } = solaredge;

const ENPHASE_KEY = '<>';
const SOLAREDGE_KEY = '<>';

(async () => {
  const enphaseProd = await getEnphaseDailyProduction({
    vendorId: 786942,
    apiKey: ENPHASE_KEY,
    startDate: '2019-01-01',
    endDate: '2019-01-31'
  });
  console.log(enphaseProd);

  const solaredgeProd = await getSolaredgeDailyProduction({
    vendorId: 533705,
    apiKey: SOLAREDGE_KEY,
    startDate: '2019-01-01',
    endDate: '2019-01-31'
  });
  console.log(solaredgeProd);
})();
