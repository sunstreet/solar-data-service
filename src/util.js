const formatDate = d => {
  let date = d;
  if (!(d instanceof Date)) {
    date = new Date(d);
  }
  const offset = date.getTimezoneOffset();
  const formatted = new Date(date.getTime() + offset * 60 * 1000);
  return formatted.toISOString().split('T')[0];
};

module.exports = {
  formatDate
};
