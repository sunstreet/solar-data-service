const fetch = require('cross-fetch');
const { formatDate } = require('./util');

const API_BASE = 'https://api.enphaseenergy.com/api/v2';

const getDailyProduction = async ({ vendorId, startDate, endDate, apiKey }) => {
  const params = {
    key: apiKey
  };
  if (typeof startDate !== 'undefined' && startDate !== null) {
    params.start_date = formatDate(startDate);
  }
  if (typeof endDate !== 'undefined' && endDate !== null) {
    params.end_date = formatDate(endDate);
  }
  const urlParams = `?${new URLSearchParams(params)}`;
  return (await fetch(
    `${API_BASE}/systems/${vendorId}/energy_lifetime${urlParams}`
  )).json();
};

module.exports = {
  getDailyProduction
};
