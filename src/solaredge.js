const fetch = require('cross-fetch');
const { formatDate } = require('./util');

const API_BASE = 'https://monitoringapi.solaredge.com/';

const getDailyProduction = async ({ vendorId, startDate, endDate, apiKey }) => {
  const params = {
    api_key: apiKey,
    timeUnit: 'DAY'
  };
  if (typeof startDate !== 'undefined' && startDate !== null) {
    params.startDate = formatDate(startDate);
  }
  if (typeof endDate !== 'undefined' && endDate !== null) {
    params.endDate = formatDate(endDate);
  }
  const urlParams = `?${new URLSearchParams(params)}`;
  const res = await (await fetch(
    `${API_BASE}/site/${vendorId}/energy${urlParams}`
  )).json();

  return {
    system_id: vendorId,
    start_date: startDate,
    production: res.energy.values.map(v => v.value)
  };
};

module.exports = {
  getDailyProduction
};
